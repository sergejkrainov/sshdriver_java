/*

Created by safonov-dv on 28.11.2018.

*/

package sshdriver;

import ansiterm.AnsiTerm;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.PTYMode;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Command;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.EnumMap;
import java.util.concurrent.TimeUnit;


public class GroupTransactions {
    private SSHClient ssh;
    private Session session;
    public AnsiTerm term;
    private InputStream mTermIn;
    private OutputStream mTermOut;

    public static JFrame window;
    public static JEditorPane textArea;
    public static JPanel panel;

    public AnsiTerm getTerm() {
        return term;
    }

    public static void createWidget () {

        //Создадим окно и установим заголовок
        window = new JFrame("Caption");

        //Текстовое поле
        textArea = new JEditorPane();
        textArea.setBackground(Color.WHITE);
        textArea.setSize(1200, 550);
        //textArea.setColumns(85);
        //textArea.setRows(35);

        //Создадим панель
        panel = new JPanel();

        //Добавим кнопки и поля на панель
        panel.add(textArea);

        //Добавим панель в окно
        window.getContentPane().add(panel);

        window.pack();

        //Разместим программу по центру
        window.setLocationRelativeTo(null);
        window.setVisible(true);

        window.setSize(1300, 700);

    }



    public void drawOutputStream(String stream) throws InterruptedException {

        textArea.setText(stream);
        Thread.sleep(500);
    }

    public GroupTransactions(String host, String user, String password, String command) throws IOException {
        term = new AnsiTerm(25, 80);
        ssh = new SSHClient();
        ssh.addHostKeyVerifier(new PromiscuousVerifier());
        ssh.connect(host);
        ssh.authPassword(user, password);
        session = ssh.startSession();
        session.allocatePTY("at386", 80, 25, 0, 0, new EnumMap<PTYMode, Integer>(PTYMode.class));
        Command cmd = session.exec(command);
        mTermIn = cmd.getInputStream();
        mTermOut = cmd.getOutputStream();
    }

    public void type(String s) throws IOException {
        mTermOut.write(s.getBytes("ibm866"));
        mTermOut.flush();
    }

    public void type(char s) throws IOException {
        mTermOut.write(s);
        mTermOut.flush();
    }

    public void refresh() throws IOException {
        int length = mTermIn.available();
        byte[] buffer = new byte[length];
        if(length > 0)
            length = mTermIn.read(buffer, 0, length);
        String instr = new String(buffer, "ibm866");
        term.feed(instr);
    }

    public void wait(String pattern, int timeout) throws InterruptedException, IOException {
        Thread.sleep(500);
        if (!exists(pattern, timeout)) {
            //System.out.println('\n' + term.getColorScreen());
            close();
            throw new IOException(pattern + "\n" + term.getString());
        }
    }

    public boolean find(String pattern, int timeout) throws InterruptedException, IOException {
        Thread.sleep(500);
        if (!exists(pattern, timeout)) {
            //System.out.println('\n' + term.getColorScreen());
            return false;
        } else {
            return true;
        }
    }

    private boolean exists(String pattern, int timeout) throws InterruptedException, IOException {
        long start_time = System.currentTimeMillis();
        long elapsed_sec = 0;
        while(timeout == -1 || timeout >= elapsed_sec) {
            int i = find(pattern);
            if(i >= 0)
                return true;
            TimeUnit.MILLISECONDS.sleep(100);
            elapsed_sec = (System.currentTimeMillis() - start_time) / 1000;
        }
        return false;
    }

    private int find(String pattern) throws IOException {
        refresh();
        String string_repr = term.getString();
        return string_repr.indexOf(pattern);
    }

    public void close() throws IOException {
        session.close();
        ssh.disconnect();
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        //createWidget ();

        GroupTransactions drv = new GroupTransactions("10.26.24.61", "samsonov-ani", "!!!QQQ222", "bq41dtrps");


        drv.wait("Ваш индивидуальный код", 2);
        System.out.println(drv.term.getColorScreen());

        drv.type("admsan");
        drv.type(Key.ENTER);
        drv.wait("Ваш паpоль", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type("1234");
        drv.wait("Ваш паpоль", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type(Key.ENTER);
        drv.wait("Время изменения схемы", 1);
        drv.type(Key.ENTER);
        System.out.println(drv.term.getColorScreen());
        drv.wait("Необходимо", 1);
        drv.type(Key.ENTER);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type(Key.ENTER);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type(Key.ENTER);
        drv.wait("Кредитные договоры", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.RIGHT);
        drv.wait("Депозиты", 1);
        drv.type(Key.RIGHT);
        drv.wait("Платежные документы", 1);
        drv.type(Key.ENTER);
        drv.wait("Документы", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Дата", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Выберите пункт", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Номер", 5);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.INSERT);
        drv.wait("Код транзакции", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.F7);
        drv.wait("Код транзакции", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type("НачПрКрВкм");
        drv.wait("Код транзакции", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Код транзакции", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Номер первого документа", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Плановая дата документа", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("НачПрКрВкм", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.DOWN);
        drv.wait("НачПрКрВкм", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.DOWN);
        drv.wait("НачПрКрВкм", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.CTRL_ENTER_CH);
        drv.wait("PreView", 10);
        System.out.println(drv.term.getColorScreen());

        drv.close();

    }
}
