package sshdriver;

/**
 * Created by safonov-dv on 29.11.2018.
 */
public  class Key {
    public static String ENTER = "\r";
    //public static String CTRL_ENTER = "\u0000A";
    public static char CTRL_ENTER_CH = 0x0a;
    public static char ENTER_CH = 13;
    public static String DOWN = "\u001B[B";
    public static String UP = "\u001B[A";
    public static String RIGHT = "\u001B[C";
    public static String LEFT = "\u001B[D";
    public static String SPACE = "\u0020";
    public static String DELETE = "\u007F";
    public static String TAB = "\u0009";

    public static String CTRL_A = "\u0001";

    public static String F1 = "\u001bOP";
    public static String F2 = "\u001bOQ";
    public static String F3 = "\u001bOR";
    public static String F4 = "\u001bOS";
    public static String F5 = "\u001bOT";
    public static String F6 = "\u001bOU";
    public static String F7 = "\u001bOV";
    public static String F8 = "\u001bOW";
    public static String F9 = "\u001bOX";

    public static String INSERT = "\u001b[@";

}
