package pages;

import org.junit.Assert;
import sshdriver.Key;
import sshdriver.TermDriver;

import java.io.IOException;

public class SelectModule {

    public void selectModule(TermDriver drv) throws IOException, InterruptedException {


        drv.wait("Выберите модуль", 2);
        System.out.println(drv.getTerm().getColorScreen());
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type(Key.ENTER);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        drv.type(Key.DOWN);
        drv.wait("Выберите модуль", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type(Key.ENTER);
        drv.wait("Кредитные договоры", 1);

        drv.type(Key.ENTER);
        drv.wait("Кредитные договоры", 1);
        System.out.println(drv.term.getColorScreen());
        drv.type(Key.ENTER);
        drv.wait("Номер договора", 1);
        System.out.println(drv.term.getColorScreen());

    }
}
