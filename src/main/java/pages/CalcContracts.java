package pages;

import sshdriver.Key;
import sshdriver.TermDriver;

import java.io.IOException;

public class CalcContracts {

    public void сalc_сontracts(TermDriver drv) throws IOException, InterruptedException {


        drv.wait("Номер договора", 2);
        drv.type(Key.CTRL_A);
        drv.wait("Номер договора", 20);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.F5);
        drv.wait("Состояние на", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type("300919");
        drv.wait("Состояние на", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Выберите ответ", 1);
        System.out.println(drv.term.getColorScreen());

        drv.type(Key.ENTER);
        drv.wait("Происходит пересчёт договоров", 3600);
        System.out.println(drv.term.getColorScreen());

        drv.wait("Договор пересчитан", 3600);
        System.out.println(drv.term.getColorScreen());


        drv.close();
    }
}
