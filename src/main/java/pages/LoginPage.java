package pages;

import sshdriver.Key;
import sshdriver.TermDriver;

import java.io.IOException;

public class LoginPage {

    public void login(TermDriver drv) throws IOException, InterruptedException {

        drv.wait("Ваш индивидуальный код", 2);

        drv.type("admsan");
        drv.type(Key.ENTER);
        drv.wait("Ваш паpоль", 2);
        System.out.println(drv.getTerm().getColorScreen());
        //drv.drawOutputStream(drv.term.getColorScreen());
        drv.type("1234");
        drv.type(Key.ENTER);

    }

    public void processOK(TermDriver drv) throws IOException, InterruptedException {

        drv.wait("Время изменения схемы", 2);
        drv.type(Key.ENTER);
        System.out.println(drv.getTerm().getColorScreen());
        drv.wait("Необходимо", 2);
        drv.type(Key.ENTER);

    }
}
