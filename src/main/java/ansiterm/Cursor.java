package ansiterm;

/**
 * Created by safonov-dv on 27.11.2018.
 */
public class Cursor {
    public int x;
    public int y;

    public Cursor(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Cursor() {
        this(0, 0);
    }
}
