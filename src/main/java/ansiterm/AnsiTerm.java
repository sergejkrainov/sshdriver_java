package ansiterm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by safonov-dv on 27.11.2018.
 */
public class AnsiTerm {
    private Cursor cursor;
    private Color color;
    private int rows;
    private int cols;
    private Tile[] tiles;

    public AnsiTerm(int rows, int cols) {
        //Initializes the ansiterm with rows*cols white-on-black spaces
        this.rows = rows;
        this.cols = cols;
        tiles = new Tile[rows * cols];
        for (int i=0; i < tiles.length; i++)
            tiles[i] = new Tile();
        cursor = new Cursor();
        color = new Color();
    }

    public Tile[] getTiles(int from, int to) {
        // Returns the tileset of a section of the screen
        int len = to - from + 1;
        Tile[] sec_tls = new Tile[len];
        System.arraycopy(tiles, from, sec_tls, 0, len);
        return sec_tls;
    }

    public String getString() {
        return getString(0, rows*cols - 1);
    }

    public String getString(int from, int to) {
        // Returns the character of a section of the screen
        String s = "";
        for(Tile tile:getTiles(from, to)){
            s += tile.glyph;
        }
        return s;
    }

    public String getColorScreen() {
        String ln = "";
        for (int i = 0; i < rows; i++) {
            int fr = i * cols;
            int to = i * cols + cols - 1;
            for (Tile tile : getTiles(fr, to)){
                ln +=  "\033[" +
                       (tile.color.bold ? "1" : "0") + ";" +
                       (tile.color.bold ? tile.color.fg + 60 : tile.color.fg) + ";" +
                       tile.color.bg + "m" +
                       tile.glyph +
                       "\033[m";
            }
            ln += "\n";
        }
        return ln;
    }

    public void feed(String s) {
        //Feeds the terminal with input_str
        while(s.length() > 0) {
            CSI csi = _parse_sequence(s);
            if (csi != null) {
                s = csi.rest;
                //System.out.println(csi.command);
                //System.out.println(csi.params.length);
                _evaluate_sequence(csi);
            } else {
                char a = s.charAt(0);
                if (a == '\r')
                    cursor.x = 0;
                else if (a == '\b')
                    cursor.x -= 1;
                else if (a == '\n')
                    cursor.y += 1;
                else if (a == 0x0f || a == 0x00) {
                } else {
                    tiles[get_cursor_idx()].set(String.valueOf(a), color);
                    //System.out.print(tiles[get_cursor_idx()].glyph);
                    cursor.x += 1;
                }
                s = s.substring(1);
            }
        }
        _fix_cursor();
        tiles[get_cursor_idx()].color.bg = 102;  // bright green cursor
    }

    private void _evaluate_sequence(CSI csi) {
        /* Evaluates a sequence (i.e., this changes the state of the terminal).
            Is meant to be called with the return values from _parse_sequence as arguments. */
        int range_fr = 0, range_to = 0;
        // Translate the cursor into an index into our 1-dimensional tileset.
        int curidx = get_cursor_idx();
        // Sets cursor position
        if (csi.command.equals("H")) {
            cursor.y = csi.params[0] - 1;  //1 - based indexes
            cursor.x = csi.params[1] - 1;
            // Sets color/boldness
        } else if (csi.command.equals("m") || csi.command.equals("M")) {
            for (int num : csi.params)
                _parse_sgr(num);
            // Clears (parts of) the screen
        } else if (csi.command.equals("J")) {// From cursor to end of screen
            if (csi.params[0] == 0) {
                range_fr = curidx;
                range_to = cols - cursor.x - 1;
            }
            // From beginning to cursor
            else if (csi.params[0] == 1) {
                range_fr = 0;
                range_to = curidx;
            }
            // The whole screen
            else if (csi.params[0] == 2) {
                range_fr = 0;
                range_to = cols * rows - 1;
            } else {
                //raise Exception('Unknown argument for J parameter: ''%s (input=%r)' % (numbers, input[:20]))
            }
            for (int i = range_fr; i < range_to; i++)
                tiles[i].reset(new Color(color.bg));
            // Clears (parts of) the line
        } else if (csi.command.equals("K")) {//From cursor to end of line
            if (csi.params[0] == 0) {
                range_fr = curidx;
                range_to = curidx + cols - cursor.x - 1;
            }
            // From beginning of line to cursor
            else if (csi.params[0] == 1) {
                range_fr = curidx % cols;
                range_to = curidx;
            }
            // The whole line
            else if (csi.params[0] == 2) {
                range_fr = curidx % cols;
                range_to = curidx % cols + cols;
            } else {
                //raise Exception('Unknown argument for K parameter: ''%s (input=%r)' % (numbers, input[:20]))
            }
            for (int i = range_fr; i < range_to; i++) {
                tiles[i].reset(new Color(color.bg));
            }
            // Move cursor up
        } else if (csi.command.equals("A")) {
            cursor.y -= csi.params[0];
            // Move cursor down
        } else if (csi.command.equals("B")) {
            cursor.y += csi.params[0];
            // Move cursor right
        } else if (csi.command.equals("C")) {
            cursor.x += csi.params[0];
            // Move cursor left
        } else if (csi.command.equals("D")) {
            cursor.x -= csi.params[0];
        } else if (csi.command.equals("r") || csi.command.equals("l")) {
        }
        //raise Exception('Unknown escape code: char=%r numbers=%r' % (char, numbers))
    }

    private void _parse_sgr(int num) {
        // Обработчик SGR-кода <escape code>n[;k]m, который изменяет графическое представление
        if(30 <= num && num <= 37)  // цвет шрифта
            color.fg = num;
        else if(40 <= num && num <= 47)  // цвет фона
            color.bg = num;
        else if(num == 1)
            color.bold = true;
        else if(num == 7)
            color.reverse = true;
        else if(num == 0) {
            color.fg = 37;
            color.bg = 40;
            color.bold = false;
            color.reverse = false;
        }
        else {
            //System.out.print("ERR SGR: ");
            //System.out.println(num);
        }
    }

    private CSI _parse_sequence(String s) {
        Pattern p = Pattern.compile("^\\x1b\\[?([\\d;]*)(\\w)");
        if(s.charAt(0) != 0x1b) {
            return null;
        }
        Matcher m = p.matcher(s);
        if(!m.find()) {
            //raise Exception('Invalid escape sequence, input_str[:20]=%r'
        }
        String args = m.group(1);
        String ch = m.group(2);
        //System.out.println(ch + " " + args);
        int[] numbers;
        String[] arg = null;
        // If arguments are omitted, add the default argument for this sequence.
        if(args.length() == 0) {
            if ("ABCDEFSTf".contains(ch)) {
                numbers = new int[]{1};
            }
            else if (ch.equals("H")) {
                numbers = new int[]{1, 1};
            }
            else {
                numbers = new int[]{0};
            }
        }
        else {
            arg = args.split(";");
            numbers = new int[arg.length];
            for (int i = 0; i < arg.length; i++) {
                numbers[i] = Integer.parseInt(arg[i]);
            }
        }
        return new CSI(ch, numbers, s.substring(m.end()));
    }

    private int get_cursor_idx() {
        return cursor.y * cols + cursor.x;
    }

    private void _fix_cursor() {
        // Makes sure the cursor are within the boundaries of the current terminal size.
        while (cursor.x >= cols) {
            cursor.y += 1;
            cursor.x = cursor.x - cols;
        }
        if (cursor.y >= rows)
            cursor.y = rows - 1;
    }


    public static void main(String[] args) {
        String instr = "\033[0;37;44m\033[25;1H\033[K\033[25;79H\033[12;21H\033[0;34;47m                                        \033[13;21H                                        \033[14;21H                                        \033[25;79H\033[10;21H                                        \033[11;21H                                        \033[25;79H\033[9;20H\033[0;37;44m                                     3668 \033[10;20H                                     1166 \033[11;20H                                     5140 \033[12;20H                                     6597 \033[13;20H\"                                     297 \033[14;20HА\"                                   3930 \033[15;20H                                     2833 \033[25;79H\033[1;1H┌──────────────────────────────────────────────────────────────────────────────┐\033[2;1H│  Отношение к банку: клиент             Клиент N: 0           0               \033[0;34;47m \033[3;1H\033[0;37;44m│Дата\033[3;7Hзаведения:         \033[3;40HДата\033[3;45Hухода:\033[4;1H│        Статус:                                                               \033[0;34;47m \033[5;2H\033[0;37;44m      Название:         \033[5;58H   \033[5;62H        \033[5;79H \033[0;34;47m \033[6;2H\033[0;37;44mКраткое наимен:                                   \033[6;53H        \033[6;62H                \033[6;79H \033[0;34;47m \033[7;2H\033[0;37;44m   \033[7;6HКод страны:  \033[7;20H         \033[7;57H    \033[7;80H\033[0;34;47m \033[8;2H\033[0;37;44m   \033[8;6H         \033[8;57H    \033[8;80H\033[0;34;47m \033[9;2H\033[0;37;44mРегион ГНИ:\033[9;22HРегион:\033[9;45HРайон:\033[9;57H    \033[9;62H            \033[9;80H\033[0;34;47m \033[10;2H\033[0;37;44m   \033[10;6H Город:\033[10;41HНас.пункт:\033[10;57H    \033[10;80H\033[0;34;47m \033[11;2H\033[0;37;44m   \033[11;6H Улица:\033[11;57H    \033[11;80H\033[0;34;47m \033[12;2H\033[0;37;44m   \033[12;6H   Дом:\033[12;20HСтр.:\033[12;32HКорпус:\033[12;46HОфис:\033[12;57H    \033[12;62HИндекс:\033[12;70H000000\033[12;80H\033[0;34;47m \033[13;2H\033[0;37;44m   \033[13;6H               \033[13;58H   \033[13;62H       \033[13;80H\033[0;34;47m \033[14;2H\033[0;37;44m   \033[14;6H   Телефон:     \033[14;42HТелефакс:\033[14;57H    \033[14;80H\033[0;34;47m \033[15;2H\033[0;37;44m Нал.инспекция:\033[15;47HИНН:\033[15;57H    \033[15;80H\033[0;34;47m \033[16;2H\033[0;37;44m   \033[16;6H     ogrn$:\033[16;45HОКВЭД:\033[16;57H    \033[16;80H\033[0;34;47m \033[17;2H\033[0;37;44m   \033[17;6H  Код ОКПО:\033[17;57H    \033[17;80H\033[0;34;47m \033[18;2H\033[0;37;44m   \033[18;6H       КПП:\033[18;31HФорма\033[18;37Hсобственности:\033[18;57H    \033[18;62H            \033[18;80H\033[0;34;47m \033[19;2H\033[0;37;44m   \033[19;6H   Субъект:\033[19;47HКод:\033[19;57H    \033[19;80H\033[0;34;47m \033[20;2H\033[0;37;44mИдентиф. банка:\033[20;37HКорсчет\033[20;45Hв\033[20;47HРКЦ:\033[20;57H    \033[20;80H\033[0;34;47m \033[21;2H\033[0;37;44m   \033[21;6H  Web-сайт: \033[21;36HРасчетный\033[21;46Hсчет:\033[21;58H   \033[21;80H\033[0;34;47m \033[22;2H\033[0;37;44m──────────────────────────────────────────────────────────────────────────────\033[25;79H\033[2;52H?\033[2;64H \033[3;18H23/11/18\033[7;18HRUS\033[9;11H  \033[9;22H       \033[9;45H      \033[10;11H  \033[10;41H          \033[15;18H0000\033[15;52H1123123332\033[19;52HМФО-9\033[25;79H\033[2;23H\033[0;30;46mклиент    \033[25;79H\033[2;52H?            \033[25;79H\033[2;52H\033[3;18H23/11/18     \033[2;52H\033[3;52H             \033[2;52H\033[3;54H/\033[3;57H/\033[4;18H                                     \033[3;58H\033[5;18H                                                            \033[3;58H\033[6;18H                                                            \033[3;58H\033[7;18HRUS      \033[3;58H\033[8;2H                                                                              \033[3;58H\033[9;11H                                                                     \033[3;58H\033[10;11H                                                       \033[3;58H\033[10;66H      \033[3;58H\033[11;14H                                                              \033[3;58H\033[12;14H      \033[3;58H\033[12;26H     \033[3;58H\033[12;40H      \033[3;58H\033[12;52H          \033[3;58H\033[12;70H000000    \033[3;58H\033[12;75H\033[3;80H\033[0;37;44m\033[0;34;47m \033[0;37;44m\033[11;80H\033[K\033[13;2H\033[0;30;46m                                                                              \033[11;80H\033[14;18H                    \033[11;80H\033[14;52H                  \033[11;80H\033[15;18H0000              \033[11;80H\033[15;52H1123123332        \033[11;80H\033[16;18H               \033[11;80H\033[16;52H          \033[11;80H\033[17;18H                \033[11;80H\033[18;18H          \033[11;80H\033[18;52H          \033[11;80H\033[18;62H                 \033[11;80H\033[19;18H          \033[11;80H\033[19;52HМФО-9         \033[11;80H\033[20;18H                  \033[11;80H\033[20;52H                           \033[11;80H\033[21;18H                  \033[11;80H\033[21;52H                           \033[11;80H\033[2;23H\033[0;37;44mклиент    \033[11;80H\033[2;52H?            \033[11;80H\033[3;18H23/11/18     \033[11;80H\033[3;52H  /  /       \033[11;80H\033[3;54H \033[3;57H \033[4;18H                                     \033[3;58H\033[5;18H                                                            \033[3;58H\033[6;18H                                                            \033[3;58H\033[7;18HRUS      \033[3;58H\033[8;2H                                                                              \033[3;58H\033[9;11H                                                                     \033[3;58H\033[10;11H                                                       \033[3;58H\033[10;66H      \033[3;58H\033[11;14H\033[K\033[3;58H\033[12;14H      \033[3;58H\033[12;26H     \033[3;58H\033[12;40H      \033[3;58H\033[12;52H          \033[3;58H\033[12;70H000000    \033[3;58H\033[13;2H                                                                              \033[3;58H\033[14;18H                    \033[3;58H\033[14;52H                  \033[3;58H\033[15;18H0000              \033[3;58H\033[15;52H1123123332        \033[3;58H\033[16;18H               \033[3;58H\033[16;52H          \033[3;58H\033[17;18H                \033[3;58H\033[18;18H          \033[3;58H\033[18;52H          \033[3;58H\033[18;62H                 \033[3;58H\033[19;18H          \033[3;58H\033[19;52HМФО-9         \033[3;58H\033[20;18H                  \033[3;58H\033[20;52H                           \033[3;58H\033[21;18H                  \033[3;58H\033[21;52H                           \033[3;58H\033[2;23H\033[0;30;46mклиент    \033[3;58H\033[2;46H\033[0;37;44m  \033[2;49H  \033[3;58H\033[2;46Hunk$:\033[3;58H\033[2;46Hнт N\033[3;58H\033[2;46H  \033[2;49H  \033[3;58H\033[2;47Hunk$:\033[3;58H\033[2;47HУНК: \033[3;58H\033[2;52H \033[2;52H?\033[2;52H \033[2;52H?\033[2;52H\033[0;30;46m?            \033[2;53H\033[2;46H\033[0;37;44mнт N\033[2;53H\033[2;52H?           \033[2;52H\033[0;30;46m?           \033[3;18H23/11/18     \033[2;64H\033[4;18H                                     \033[2;64H\033[5;18H                                                            \033[2;64H\033[6;18H                                                            \033[2;64H\033[7;18HRUS      \033[2;64H\033[9;11H                                                                     \033[2;64H\033[9;11H\033[0;37;44mИ: \033[2;64H\033[9;14H    \033[9;2H      \033[9;9H    \033[9;18H\033[10;11H\033[0;30;46m                                                       \033[9;18H\033[14;18H                    \033[9;18H\033[14;52H                  \033[9;18H\033[15;18H0000              \033[9;18H\033[15;52H1123123332        \033[9;18H\033[16;11H\033[0;37;44m      \033[9;18H\033[16;11Hogrn$:\033[9;18H\033[16;11H      \033[9;18H\033[16;12Hogrn$:\033[9;18H\033[16;12HОГРН: \033[9;18H\033[16;18H\033[0;30;46m               \033[9;18H\033[16;52H          \033[9;18H\033[17;18H                \033[9;18H\033[18;18H          \033[9;18H\033[18;38H\033[0;37;44mКод ФормСобс\033[9;18H\033[18;52H\033[0;30;46m          \033[9;18H\033[19;18H          \033[9;18H\033[19;52HМФО-9         \033[9;18H\033[20;18H                  \033[9;18H\033[20;52H                           \033[9;18H\033[21;8H\033[0;37;44m         \033[9;18H\033[21;8HWe\033[9;18H\033[21;8H  \033[9;18H\033[21;16HWe\033[9;18H\033[21;16H: \033[9;18H\033[21;16H \033[9;18H\033[21;8H:\033[9;18H\033[21;8HWeb-сайт:\033[9;18H\033[21;18H\033[0;30;46m                  \033[9;18H\033[21;52H                           \033[9;18H\033[5;8H\033[0;37;44m\033[1;36;44mНазвание: \033[9;18H\033[9;22H        \033[9;18H\033[14;9HТелефон: \033[9;18H\033[15;47HИНН: \033[9;18H\033[16;45HОКВЭД: \033[9;18H\033[22;2H\033[0;37;44m\033[0;34;47m F1│Shift+F1 \033[0;37;44m─────────────────────────────────────────────────────────────────\033[11;80H\033[0;34;47m \033[22;80H\033[0;37;44m\033[11;80H\033[K\033[22;80H\033[1;27H\033[1;37;44m[cust-corp: ЮР ЛИЦА - ввод]\033[22;80H\033[11;80H\033[0;37;44m\033[0;34;47m \033[0;37;44m\033[3;80H\033[K\033[22;80H\033[2;52H             \033[22;80H\033[3;80H\033[0;34;47m \033[0;37;44m\033[11;80H\033[K\033[22;80H\033[2;47H \033[2;49H  \033[22;80H\033[18;52H          \033[22;80H\033[18;31H     \033[18;37H    \033[18;42H         \033[22;80H\033[9;18H                                                              \033[22;80H\033[10;11H                                                       \033[22;80H\033[10;9H  \033[22;80H\033[9;14H\033[0;30;46m    \033[22;80H\033[9;2H\033[0;37;44mРегион\033[9;9HГНИ:\033[22;80H\033[2;47Hт\033[2;49HN:\033[22;80H\033[2;52H\033[0;30;46m?           \033[9;22H\033[0;37;44m\033[1;36;44mРегион: \033[2;64H\033[9;45H\033[0;37;44mРайон:\033[2;64H\033[10;9Hрод:\033[2;64H\033[10;41HНас.пункт:\033[2;64H\033[18;38HКод\033[18;42HФормСобс:\033[2;64H\033[18;52H\033[0;30;46m          \033[8;2H\033[0;37;44m──────────────────────────────────────────────────────────────────────────────\033[13;2H──────────────────────────────────────────────────────────────────────────────\033[2;23H\033[11;80H\033[0;34;47m \033[0;37;44m\033[3;80H\033[K\033[2;23H\033[25;79H\033[25;1HЯвляется\033[25;10Hли\033[25;13Hсубъект\033[25;21Hклиентом\033[25;30Hбанка.\033[25;79H\033[22;2H\033[0;34;47m F1│Shift+F1 \033[0;37;44m─────────────────────────────────────────────────────────────────\033[25;79H\033[2;23H\033[25;79H\033[22;2H\033[0;34;47m F1│Shift+F1 \033[0;37;44m─────────────────────────────────────────────────────────────────\033[25;79H\033[2;23H";
        AnsiTerm term = new AnsiTerm(25, 80);
        term.feed(instr);
        System.out.println(term.getColorScreen());
        for (int y = 0; y < 25; y++) {
            //System.out.println(term.getString(y * 80, y * 80 + 79));
        }
    }


}
