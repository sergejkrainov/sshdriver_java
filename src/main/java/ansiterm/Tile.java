package ansiterm;

/**
 * Created by safonov-dv on 27.11.2018.
 */
public class Tile {
    public String glyph;
    public Color color;

    public Tile() {
        reset();
    }
    public void reset(Color color) {
        this.glyph = " ";
        this.color = color;
    }
    public void reset() {
        reset(new  Color());
    }
    public void set(String glyph, Color color) {
        this.glyph = glyph;
        this.color.fg = color.fg;
        this.color.bg = color.bg;
        this.color.reverse = color.reverse;
        this.color.bold = color.bold;
    }
}
