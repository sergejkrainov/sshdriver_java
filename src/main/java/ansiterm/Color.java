package ansiterm;

/**
 * Created by safonov-dv on 27.11.2018.
 */
public class Color {
    public int fg;
    public int bg;
    public Boolean bold;
    public Boolean reverse;

    public Color(int fg, int bg, Boolean bold, Boolean reverse) {
        this.fg = fg;
        this.bg = bg;
        this.bold = bold;
        this.reverse = reverse;
    }

    public Color() {
        this(37, 40, false, false);
    }

    public Color(int bg) {
        this(37, bg, false, false);
    }
}
