package ansiterm;

/**
 * Created by safonov-dv on 27.11.2018.
 */
public class CSI {
    String command;
    int[] params;
    String rest;

    public CSI(String command, int[] params, String rest) {
        this.command = command;
        this.params = params;
        this.rest = rest;
    }
}
